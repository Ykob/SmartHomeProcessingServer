#include <Arduino.h>
#include <EEPROM.h>

#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>

#include <SocketIoClient.h>

#define USE_SERIAL Serial

ESP8266WiFiMulti WiFiMulti;
SocketIoClient webSocket;

const char* ID = "";
int state = 0;
int lastState = 0;
const int eeprom_size = 500;
char eeprom_buffer[eeprom_size];


void changeState(const char * payload, size_t length) {
  USE_SERIAL.printf("New state: %s\n", payload);
  String s = String(payload);
  state = s.toInt();
}

void connectEvent(const char * payload, size_t length){
  if(ID = ""){
    ID = NULL;  
  }
  webSocket.emit("identificate", ID);
}

void setNewID(const char * payload, size_t length){
  ID = payload;
  USE_SERIAL.printf("New ID: %s\n", ID);
  for(int i = 0; i < (strlen(ID) - 1); i++){
    
      EEPROM.write(i, ID[i]);
  }
}

void setup() {
    USE_SERIAL.begin(115200);

    //USE_SERIAL.setDebugOutput(true);

      for(uint8_t t = 4; t > 0; t--) {
          USE_SERIAL.printf("[SETUP] BOOT WAIT %d...\n", t);
          USE_SERIAL.flush();
          delay(1000);
      }

    for(int i = 0; i < (eeprom_size - 1); i++){
  
      eeprom_buffer[i] = EEPROM.read(i);
  
    }
    ID = eeprom_buffer;

    WiFiMulti.addAP("DMNET2", "Poczopek10#$");

    while(WiFiMulti.run() != WL_CONNECTED) {
        delay(100);
    }

    webSocket.on("changeState", changeState);
    webSocket.on("connect", connectEvent);
    webSocket.on("newID", setNewID);
    
    webSocket.begin("192.168.0.1", 3000);
}

void loop() {
    webSocket.loop();
    if(lastState!=state){
      String str = String(state);
      const char *toSend = str.c_str();
      webSocket.emit("data", toSend);
      lastState = state;
    }
}

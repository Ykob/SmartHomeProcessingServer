var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var ardrone = require('ar-drone');
var bodyParser = require('body-parser');

app.use(bodyParser.json());

//controllers and models
var ctrl = require('./deviceController/deviceCtrl');

//dynamic variables
var addNewDevice = require('./deviceController/deviceCtrl.js');

app.use('/', express.static('public'));

ctrl(app, io, ardrone);

http.listen(3000, function() {
    console.log("Server's up'n running");
});

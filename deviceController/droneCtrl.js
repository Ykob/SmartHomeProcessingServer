module.exports = function(app, ardrone){

		var client = ardrone.createClient({'ip':'192.168.1.169'});
		client.on('navdata', (nawdata)=>{
			module.navdata = nawdata;
		});

		var module = {
		test : function(){
			return 'yolo';
		},
	 	takeoff : function(){
	 		console.log('takeoff');
	 		client.takeoff();
	 	},
	 	doaflip : function(){
	 		console.log('flip!')
	 		client.animate('flipBehind', 15);
	 	},
	 	land : function(){
	 		console.log('land');
	 		client.land();
	 	}
	 	};
	return module;
}
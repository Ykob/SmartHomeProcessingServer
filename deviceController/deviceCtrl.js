module.exports = function(app, ioInst, droneOmg){
var express = require('express');
var router = express.Router();
var io = ioInst;
var drone = require('./droneCtrl.js')(app, droneOmg);

io.set('heartbeat timeout', 4000); 
io.set('heartbeat interval', 10000);

var addNewDevice = false;
var newDeviceName = "Salon-rollup";
var newDeviceFunction = "percent";
var newDeviceGroup = "LivingRoom";
var resetDeviceData = false;

var socketList = [];
var devices = [];
var webapp = [];
var password = "password";

	io.on('connection', (socket) => {
		console.log('new connection');
		var stdin = process.openStdin();
		stdin.addListener("data", (data)=>{
			socket.emit("data", data.toString().trim());
		});
		socket.on('identificate', (identification) => {
			if(identification == "webapp"){
					console.log('Webapp connected!');
					socket.join('webapp');
					
					var toSend = [];
					devices.forEach(device => {
							toSend.push({'name':device.id, 'group':device.group, 'function':device.function});
					});
					console.log(toSend);	
					socket.emit('connectedDevices', toSend);
				}
			else if(identification.password == password){
				
					devices.push({"socket":socket, "id":identification.id});
					socket.emit('passOK');
					console.log('device connected with id: ' + identification.id);	

					if(resetDeviceData==true){
						socket.emit('newID', {"id":newDeviceName, "group": newDeviceGroup, "function":newDeviceFunction, "password": password});
					}
					
			}
			else if(identification.password != password && addNewDevice == true){
				socket.emit('newID', {"id":newDeviceName, "group": newDeviceGroup, "function":newDeviceFunction, "password": password});
				console.log('NEW device added!');
				socket.disconnect(true);
			}
			else{
				socket.disconnect(true);
				console.log('socket failed authentication');
			}
		});

		socket.on('deviceData', (payload) => {				//THIS EVENT HAPPENS AFTER IDENTIFICATION
			console.log('data from device\/');
			console.log(payload);
			var device = findDevice(socket);
			device.function = payload.function;
			device.group = payload.group;
		});
		
		socket.on('newDataFromDevice', (payload) => {		//THIS EVENT IS COMING FROM DEVICE
			console.log('new data arrived: ');
			console.log(payload);
				var myDevice = findDevice(socket);
				if(myDevice){
					myDevice['data'] = payload.data;	
				if(webapp){
					var toSend = prepareToSend(myDevice);
					console.log('New data was send to webapp: ');
					socket.to('webapp').emit('newData', toSend);
				}
				else{
						console.log('no webapp conected :(');
				}
			}
		});		

		socket.on('changeDeviceState', (dataPck) => {	//THIS EVENT IS COMING FROM WEBAPP
			console.log('changing device state...');
			console.log(dataPck);
			var interestingDevice = findDevice(dataPck.name);
			if(!!interestingDevice) interestingDevice['socket'].emit('data', dataPck.newState);
			else console.log('device not connected');
		});

		socket.on('drone', (data)=>{
			drone[data.figure]();
		})
		
		socket.on('disconnect', () => {
				if(!!findDevice(socket)) {
					console.log(findDevice(socket).id+ ' disconnection!');
					devices.splice(devices.indexOf(devices.find(device => {return device.socket == socket})), 1);
				}
				else console.log("webapp disconnected");
		});
	});
	
	
	
	var prepareToSend = function(device){
		return {'name': device.id, 'data':device.data};
	}
	var findDevice = function(toFind){
		return devices.find(device => {
			return device.id == toFind||device.socket == toFind;
		})
	}


}

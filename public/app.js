angular.module("smartHome", ["ngRoute"])
	.config(function($routeProvider){
		$routeProvider
		.when("/dashboard", {
			controller:"DashCtrl",
			templateUrl:"./pages/dashboard/dashboard.html"
		})
		.when("/login", {
			controller:"LoginCtrl",
			templateUrl:"./pages/login/login.html",
		})
		.when("/room/:roomGroup", {
			controller:"roomGroupController",
			templateUrl:"./pages/roomGroup/roomGroup.html"
		})
		.when("/drone", {
			controller:"droneCtrl",
			templateUrl:"./pages/drone/drone.html"
		})
		.otherwise('/login');	
	})
	.run(function($rootScope, socket){
		$rootScope.$on('$stateChangeStart', 
			function(event, toState, toParams, fromState, fromParams){ 
    			if(!socket.isLogged){
    				event.preventDefault(); 
    			}
			})
	});
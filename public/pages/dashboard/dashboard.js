angular.module('smartHome')
	.controller('DashCtrl', function($scope, $interval, socket, devices, $location, $route, $rootScope){
		
		$scope.devices = devices.devicesList();
		$scope.groups = $scope.devices.reduce(function(groups, item) {
		    const val = item['group'];
		    groups[val] = groups[val] || [];
		    groups[val].push(item);
		    return groups;
		}, {});
		console.log($scope.groups);

		
		$scope.$on('connectedDevices', (event, connectedDevices) => {
			$scope.devices = connectedDevices;
			//$scope.groups = [...new Set(connectedDevices.map(device => device.group))];
			//console.log(devices.devicesList());
			$scope.groups = $scope.devices.reduce(function(groups, item) {
				const val = item['group'];
				groups[val] = groups[val] || [];
				groups[val].push(item);
				return groups;
			}, {});
			console.log($scope.groups);
		});

		var sliders = document.getElementsByClassName("rangeSlider")

		for (slider of sliders) {
			slider.defaultValue = 0;
		}

		$scope.state = {};
		$scope.showLedControl=0;		

		$scope.goToRoom = function(group){
			$location.path(group);
		};
		$scope.$on('newData', (event, newData)=>{
			$scope.state[newData.name] = newData.data;
			console.log($scope.state[newData.name]);
		});

		$scope.action = function(deviceName){
			console.log($scope.state[deviceName]);
			devices.action(deviceName, $scope.state[deviceName]);			//motorCtrl, {motor:"1", direction:"1s"}
		};


		$scope.clock = Date.now();
		$interval(function () { $scope.clock = Date.now(); }, 1000);
		
		$scope.addNewGroup = function(newGroupName){
			Object.assign($scope.groups, {[newGroupName]:{name:newGroupName}});
		};

		$scope.createPayloadForMotor = function(deviceName, motorNumber, motorDirection){
			console.log(deviceName + " " + motorNumber + " " + motorDirection);
			devices.action(deviceName, {"motor":motorNumber, "direction":motorDirection});
		}

		$scope.newDevice = {
			group:"",
			function:"",
			name:""
		}
		
		$scope.addNewDevice = function(){
			socket.addNewDevice(newDevice).then(function(suc){
				console.log('New device ready to add!');
			},function(err){
				console.log(err);
			});
		}
	});
angular.module('smartHome')
	.controller('LoginCtrl', function($scope, socket){
		var init = function(){
			$scope.user = {
				pin:""
			}
		}
		$scope.login = function(){
			socket.login($scope.user);
			init();
		}

	});
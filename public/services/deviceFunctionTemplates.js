angular.module("smartHome")
	.service('templates', function(){

		var switchTemplate = '<div class="box">'+
				'<p style="">{{device.name}}</p>'+
				'<div id="stay-in-place">'+
					'{{device.tileContent}}'+
				'</div>'+

				'<div id="move-in-to-place">'+
					'{{device.status}}'+
				'</div>'+
			'</div>';

		return{ 
			getTemplate: function(template){
				switch(template){
					case 'Switch': 
						return switchTemplate;
						break;
					default:
						return '<p>No such device function!</p>';
						break;
				}
			}
		}

	});

angular.module('smartHome')
	.factory('socket', function($http, $rootScope, $location){
		var socket;
		var loggedUser;
		return {
			init:function(){
				socket = io.connect('192.168.43.152:3000');
			},
		    on: function (eventName, callback) {
		      socket.on(eventName, function () {  
		        var args = arguments;
		        $rootScope.$apply(function () {
		          callback.apply(socket, args);
		        });
		      });
		    },
		    emit: function (eventName, data, callback) {
		      socket.emit(eventName, data, function () {
		        var args = arguments;
		        $rootScope.$apply(function () {
		          if (callback) {
		            callback.apply(socket, args);
		          }
		        });
		      })
		    },
		    getDevices:function(){
		    	return $http.get('/device');
		    },
		    changeState:function(body){
		    	return $http.post('/device', body);
		    },
		    addNewDevice:function(body){
		    	return $http.post('/newDevice', body);
		    },
		    deleteDevice:function(deviceName){
		    	return $http.delete('/device/'+deviceName);
		    },
		    updateDevice:function(body){
		    	return $http.put('/device', body);
		    },
		    login:function(user){
		    	$http.post('/login', user).then(function(suc){
		    		$location.path('/dashboard');
		    		loggedUser=user;
		    	}, function(err){
		    		//alert('Bad Login');
		    		$location.path('/dashboard');
		    	})
		    },
		   	isLogged:function(){
		   		return !!loggedUser;
		   	}
  		};
	})

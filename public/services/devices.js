angular.module("smartHome")
	.factory('devices', function (socket, $rootScope) {

		socket.init();

		socket.on('connect', function(data){
			socket.emit('identificate', "webapp");
		});

		var drone = false;
		socket.on('drone', ()=>{drone = true});
		
		var devices = [
			{
				"group" : "Kitchen",
				"function" : "leds",
				"name" : "Led"
			},
			{
				"group" : "Kitchen",
				"function" : "percent",
				"name" : "Rollup"
			},
			{
				"group" : "LivingRoom",
				"function" : "sensor",
				"name" : "Temperature Sensor"
			},
			{
				"group" : "Bedroom1",
				"function" : "switch",
				"name" : "LightSwitch"
			},
			{
				"group" : "LivingRoom",
				"function" : "switch",
				"name" : "LivingRoomFan"
			},
			{
				"group" : 'LivingRoom',
				"function" : 'switch',
				"name" : 'toster'
			}

		];

		socket.on('connectedDevices', function(payload){
			devices = payload;
			console.log(devices);
			console.log(payload);  
			$rootScope.$broadcast('connectedDevices', devices);
		});

		socket.on('newData', function(newData){		//THIS EVENT HAPPENS WHEN NEW STATE IS COMING FROM FROM DEVICE
			console.log(newData);
			if(devices[newData.name]) {
				devices[newData.name].data = newData.data;
			}
			$rootScope.$broadcast('newData', newData);
		});

		return{
			devicesList : function(){
				return devices;
			},
			action : function(device, newData) {
				socket.emit("changeDeviceState", {"name":device, 'newState':newData});
				console.log({'name':device, 'newData':newData});
			},

			drone: function(figure){
				console.log(figure);
				socket.emit('drone', {name:'drone', figure:figure});
			}
		}
	})